         .data 0x10010000
var1:    .word 0x5  # var1 is a word (32 bit) with the ..
                     # initial value 0x55
var2:    .word 0x6
         
var3:    .word 0x11

var4:    .word 0x9

first:   .byte 's'
last:    .byte 't'

         .text
         .globl main
main:
         addu $s0, $ra, $0# save $31 in $16
         lw $t0, var1 # load var1 to t0
         lw $t1, var2 # load var2 to t1
         lw $t2, var3 # load var3 to t2
         lw $t3, var4 # load var4 to t3
         sw $t0, var4 # store t0 in var4
         sw $t1, var3 # store t1 in var3
         sw $t2, var2 # store t2 in var2
         sw $t3, var1 # store t3 in var1
         # restore now the return address in $ra and return from main
         addu $ra, $0, $s0
         # return address back in $31
         jr $ra
         # return from main
         .data 0x10010000
var1:    .word 0x5  # var1 is a word (32 bit) with the ..
                     # initial value 0x55
var2:    .word 0x6
         
var3:    .word 0x11

var4:    .word 0x9

first:   .byte 's'
last:    .byte 't'

         .text
         .globl main
main:
         addu $s0, $ra, $0# save $31 in $16
         lui $1, 4097
         lw $8, 0x0($1)
         lw $9, 0x4($1)
         lw $10, 0x8($1)
         lw $11, 0xc($1)
         sw $8, 0xc($1)
         sw $9, 0x8($1)
         sw $10,0x4($1)
         sw $11,0x0($1)
         # restore now the return address in $ra and return from main
         addu $ra, $0, $s0
         # return address back in $31
         jr $ra
         # return from main